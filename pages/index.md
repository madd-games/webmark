# WebMark

WebMark is an extremely simple tool for creating websites with pages defined in Markdown, and a single HTML template file (for navigation etc).

## How do I use it?

First you need to edit the `template.html` file. This template is used for all pages. The following substitutions are done to it:

* The text `{PAGE_TITLE}` will be replaced by the page title: the text after `#` on the first line of the page.
* The text `{PAGE_CONTENT}` will be replaced by the page content in HTML.

Then you can put all your pages in the `pages` directory. `index.md` is the home page. If a file there is named `name.md`, then
it will create the page `/name`.

That's it. Customize away.

## How can I customize the 404 page?

The file `pages/404.md` will be used if a page is not found.

## How can I update?

The `.htaccess` file, as well as all the `.php` files, form the engine's code. All other files are customizable. Therefore,
you can copy just these files during an update.