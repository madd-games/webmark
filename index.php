<?php
include("Parsedown.php");

$pageName = end(explode('/', $_SERVER['REQUEST_URI']));
if ($pageName == '')
{
    $pageName = 'index';
}

$pageData = @file_get_contents('pages/' . $pageName . '.md');
if (!$pageData)
{
    $pageData = file_get_contents('pages/404.md');
    http_response_code(404);
}

$pageTitle = $pageName;

$line = preg_split('#\r?\n#', $pageData, 2)[0];
if (substr($line, 0, 1) == '#')
{
    $pageTitle = trim(substr($line, 1));
};

$parsedown = new Parsedown();
$pageContent = $parsedown->text($pageData);

$template = file_get_contents('template.html');
$template = str_replace('{PAGE_TITLE}', $pageTitle, $template);
$template = str_replace('{PAGE_CONTENT}', $pageContent, $template);
echo $template;

?>
